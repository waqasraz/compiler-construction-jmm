
%%
%class jmmScanner
%line


LineTerminator = \r|\n|\r\n
InputCharacter = [^\r\n]
WhiteSpace     = {LineTerminator} | [ \t\f]
Identifier = [a-zA-Z_][a-zA-Z0-9_]*

%%

<YYINITIAL> {
	/* operators */
		 	"+" { return (new Yytoken("Expressions",yytext(),yyline)); }
  			"-" { return (new Yytoken("Expressions",yytext(),yyline)); }
  			"*" { return (new Yytoken("Expressions",yytext(),yyline)); }
  			"/" { return (new Yytoken("Expressions",yytext(),yyline)); }
  			"%" { return (new Yytoken("Expressions",yytext(),yyline)); }
  			"<" { return (new Yytoken("Expressions",yytext(),yyline)); }
  			">" { return (new Yytoken("Expressions",yytext(),yyline)); }
  			"<=" { return (new Yytoken("Expressions",yytext(),yyline)); }
  			">=" { return (new Yytoken("Expressions",yytext(),yyline)); }
  			"=" { return (new Yytoken("Expressions",yytext(),yyline)); }
  			"==" { return (new Yytoken("Expressions",yytext(),yyline)); }
  			"!=" { return (new Yytoken("Expressions",yytext(),yyline)); }
  			"!" { return (new Yytoken("Expressions",yytext(),yyline)); }
  			"&&" { return (new Yytoken("Expressions",yytext(),yyline)); }
  			"||" { return (new Yytoken("Expressions",yytext(),yyline)); }
  	/* keywords */
  			"break" { return (new Yytoken("Statments",yytext(),yyline)); }
  			"return" { return (new Yytoken("Statments",yytext(),yyline)); }
  			"if" { return (new Yytoken("Statments",yytext(),yyline)); }
  			"if" { return (new Yytoken("Statments",yytext(),yyline)); }
  			"else" { return (new Yytoken("Statments",yytext(),yyline)); }
  			"while" { return (new Yytoken("Statments",yytext(),yyline)); }		
  			"int" { return (new Yytoken("Type",yytext(),yyline)); }
  			"boolean" { return (new Yytoken("Type",yytext(),yyline)); }
  		
  	/* Symbols */
  			"{" { return (new Yytoken("Other",yytext(),yyline)); }
  			"}" { return (new Yytoken("Other",yytext(),yyline)); }
  			"[" { return (new Yytoken("Other",yytext(),yyline)); }
  			"]" { return (new Yytoken("Other",yytext(),yyline)); }
  			"(" { return (new Yytoken("Other",yytext(),yyline)); }
  			")" { return (new Yytoken("Other",yytext(),yyline)); }		
  			";" { return (new Yytoken("Other",yytext(),yyline)); }
  			"," { return (new Yytoken("Other",yytext(),yyline)); }
  	/* whitespace */
  			{WhiteSpace}                   { /* ignore */ }
  			
  	/* identifiers */ 
  			{Identifier}  { return (new Yytoken("Identifier",yytext(),yyline)); }		
  		}