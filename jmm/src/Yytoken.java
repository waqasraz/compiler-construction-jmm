
public class Yytoken {

	private String _type;
	private String _text;
	private int _line;
	
	public Yytoken(String type, String text, int line)
	{
		_type = type;
		_text = text;
		_line = line;
	}
	
	public String toString()
	{
		return "Type ("+_type+"): "+_text+" (line: "+_line+")"; 
	}
}
